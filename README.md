# Maps App
- Google maps interation app with Angular, Tailwind

## Installation

1. Clone the repo and `cd` into it
1. install npm packages `npm install`
1. install `npm i -g postcss-cli`
1. install `npm install tailwind, post-css --save-dev`
1. install `npm install @agm/core`
1. install `npm install @google/maps`
1. run `npm run tailwind`
1. run `npm run start` or `ng serve`
1. Visit `localhost:4200` or `localhost:4200/maps` in your browser
