import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { GoogleMapComponent } from './maps/google-map/google-map.component';
import { MapComponent } from './maps/map/map.component';

const routes: Routes = [
  { path: "", component: GoogleMapComponent },
  { path: "gmap", component: MapComponent },
  { path: "maps", component: GoogleMapComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
