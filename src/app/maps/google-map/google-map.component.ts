import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { MapsAPILoader, MouseEvent } from '@agm/core';

@Component({
  selector: 'app-google-map',
  templateUrl: './google-map.component.html',
  styleUrls: ['./google-map.component.scss']
})

export class GoogleMapComponent implements OnInit {

  title: string = 'Project';
  latitude: number;
  longitude: number;
  zoom: number;
  address: string;
  
  formattedFromAddress = '';
  formattedToAddress = '';
  recentlySearchedPlaces = [];
  recentlySearchedPlacesLatest = [];
  
  private geoCoder;

  @ViewChild('search', {static: false})
  
  public searchElementRef: ElementRef;

  constructor(
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone
  ) { }

  ngOnInit() {
    this.initProcessAutoComplete();
  }

  initProcessAutoComplete() {

    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      this.setCurrentLocation();
      
      this.geoCoder = new google.maps.Geocoder;
 
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"],
        componentRestrictions: {country: "my"}
      });

      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
 
          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
 
          //set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 12;
          this.getAddress(this.latitude, this.longitude);
          this.getrecentlySearched();

        });
      });
    });
  }

  // Get Current Location Coordinates
  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 8;
        this.getAddress(this.latitude, this.longitude);
      });
    }
  }
 
 
  markerDragEnd($event: MouseEvent) {
    console.log($event);
    this.latitude = $event.coords.lat;
    this.longitude = $event.coords.lng;
    this.getAddress(this.latitude, this.longitude);
  }
 
  getAddress(latitude, longitude) {
    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
      console.log(results);
      console.log(status);
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 12;
          this.address = results[0].formatted_address;
          
          let locValus = {
            'lat': latitude,
            'long': longitude,
            'address': this.address
          };
          this.recentlySearchedPlaces.push(locValus);

        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }
    });
  }

  getrecentlySearched() {
    this.recentlySearchedPlacesLatest  = this.recentlySearchedPlaces.slice(Math.max(this.recentlySearchedPlaces.length - 5, 1))
  }

  getSelectedLocation(lat, long) {
    this.latitude = lat;
    this.longitude = long;
    
    this.setCurrentLocation();
  }

  public handleFromAddressChange(address: any) {
    this.formattedFromAddress = address.formatted_address;  
  }
  
  public handleToAddressChange(address: any) {
    this.formattedToAddress = address.formatted_address;  
  }

  calculateDistance() {
    const mexicoCity = new google.maps.LatLng(19.432608, -99.133209);
    const jacksonville = new google.maps.LatLng(40.730610, -73.935242);
    const distance = google.maps.geometry.spherical.computeDistanceBetween(mexicoCity, jacksonville);
    console.log(distance);
  }

  // calculate distance
  // public function calculateDistance() {
  //   var origin = this.formattedFromAddress;
  //   var destination = this.formattedToAddress;
  //   var service = new google.maps.DistanceMatrixService();
  //   service.getDistanceMatrix(
  //       {
  //           origins: [origin],
  //           destinations: [destination],
  //           travelMode: google.maps.TravelMode.DRIVING,
  //           unitSystem: google.maps.UnitSystem.IMPERIAL, // miles and feet.
  //           // unitSystem: google.maps.UnitSystem.metric, // kilometers and meters.
  //           avoidHighways: false,
  //           avoidTolls: false
  //       }, callback);
  // }

}
