module.exports = {
  theme: {
    extend: {
      width: {
        "96": "24rem",
        "120": "30rem"
      }
    }
  },
  variants: {},
  plugins: []
}
